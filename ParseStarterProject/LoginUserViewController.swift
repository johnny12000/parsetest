 //
//  LoginUserViewController.swift
//  ParseStarterProject-Swift
//
//  Created by Nikola Ristic on 3/19/16.
//  Copyright © 2016 Parse. All rights reserved.
//

import UIKit
import Parse

class LoginUserViewController: UIViewController, UIAlertViewDelegate
{

    @IBOutlet var userNameTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var loginButton: UIButton!
    
    let userAgent = UserAgent()
    var user:PFUser? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
    }

    override func viewWillAppear(animated: Bool)
    {
        title = "Login"
        userNameTextField.text = ""
        passwordTextField.text = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    @IBAction func logInPressed(sender: UIButton)
    {
        do {
            let user = try userAgent.logInUser(
                userNameTextField.text!,
                password: passwordTextField.text!)
            self.user = user
            self.performSegueWithIdentifier("viewPhotosSegue", sender: nil)
        }
        catch {
            
            let alert = UIAlertController(title:  "Log in", message: "Bad login or no user", preferredStyle: UIAlertControllerStyle.Alert)
            
            let action = UIAlertAction(title: "Create new user", style: UIAlertActionStyle.Default){ (UIAlertAction) -> Void in
                self.createNewUser()
                }
            
            alert.addAction(action)
            let action1 = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler:nil)
            alert.addAction(action1)
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func createNewUser()
    {
        let alert = UIAlertController(title: "Alert", message: "Confirm password", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addTextFieldWithConfigurationHandler({(textField: UITextField!) in
            textField.placeholder = "Enter text:"
        })
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { action in
            
            let pass = alert.textFields?.first?.text
            if (pass == self.passwordTextField.text!) {
                self.userAgent.createUser(self.userNameTextField.text!, password: self.passwordTextField.text!) { usr, error in
                    if (error != nil) {
                        let alert = UIAlertController(title: "Create", message: "Could not create user", preferredStyle: UIAlertControllerStyle.Alert)
                        let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler:nil)
                        alert.addAction(action)
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                    else {
                        self.user = usr
                        self.performSegueWithIdentifier("viewPhotosSegue", sender: nil)
                    }
                }
            }
            })
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        (segue.destinationViewController as? PhotoListViewController)?.user = self.user!
    }

}
