//
//  PhotoListViewController.swift
//  ParseStarterProject-Swift
//
//  Created by Nikola Ristic on 3/19/16.
//  Copyright © 2016 Parse. All rights reserved.
//

import UIKit
import Parse
import CoreLocation

class PhotoListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,  UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate
{

    @IBOutlet var photoTable: UITableView!
    var user:PFUser?
    var photos:[AnyObject]?
    
    let photoAgent = PhotoAgent()
    
    var imagePicker:UIImagePickerController!
    var lm:CLLocationManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.photoTable.dataSource = self
        self.photoTable.delegate = self
        
        lm = CLLocationManager()
        lm.delegate = self
        
        lm.desiredAccuracy = kCLLocationAccuracyBest
        lm.requestAlwaysAuthorization()
        lm.startUpdatingHeading()
        lm.startUpdatingLocation()
        
        retrievePhotos()
        
    }
    
    override func viewWillAppear(animated: Bool)
    {
        title = "Photos"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.

    }
    
    @IBAction func newPhotoTapped(sender: UIButton)
    {
        
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        
        if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)) {
            imagePicker.sourceType = .Camera
        }
        else {
            imagePicker.sourceType = .PhotoLibrary
        }
        
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerController
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        imagePicker.dismissViewControllerAnimated(true, completion: nil)
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        AdditionalData(image!)
    }
    
    //MARK: UITableViewDataSource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return photos?.count ?? 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("photoCell", forIndexPath: indexPath)
        configureCell(cell, forRowAtIndexPath: indexPath)
        return cell
    }
    
    func configureCell(cell: UITableViewCell, forRowAtIndexPath: NSIndexPath) {
        cell.textLabel?.text = self.photos?[forRowAtIndexPath.row].objectForKey("name") as? String
        
//        let test = self.photos?[forRowAtIndexPath.row].objectForKey("photo") as? PFFile
//        test?.getDataInBackgroundWithBlock({ (data, error) -> Void in
//            if let data = data {
//                
//                cell.imageView?.image = UIImage(data: data)
//                self.photoTable.reloadRowsAtIndexPaths([forRowAtIndexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
//            }
//        })
    }
    
    //MARK: UITableViewDelegate
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    
    // MARK: - 
    
    func AdditionalData(image:UIImage)
    {
        let alert = UIAlertController(title: "Alert", message: "Image name", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addTextFieldWithConfigurationHandler({(textField: UITextField!) in
            textField.placeholder = "Enter text:"
        })
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { action in
            
            let name = alert.textFields?.first?.text
            let location = self.lm.location
            let heading = self.lm.heading
            self.createPhoto(image, title:name!, location: location, heading: heading)
            })
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func createPhoto(image:UIImage, title:String, location:CLLocation?, heading:CLHeading?)
    {
        SwiftSpinner.show("Uploading...")
        photoAgent.createPhoto(image,
            user: self.user!,
            title: title,
            location: location,
            heading: heading) {user, error in
        
                SwiftSpinner.hide()
                
                if(error != nil) {
                    let alert = UIAlertController(title: "Create", message: "Could not create photo", preferredStyle: UIAlertControllerStyle.Alert)
                    let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler:nil)
                    alert.addAction(action)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                else {
                    self.retrievePhotos()
                }
                
        }
        
    }
    
    func retrievePhotos()
    {
        photoAgent.retrievePhotos(self.user!) { photos, error in
            self.photos = photos
            self.photoTable.reloadData()
        }
    }
    
    
}
