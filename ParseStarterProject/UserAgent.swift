//
//  UserAgent.swift
//  ParseStarterProject-Swift
//
//  Created by Nikola Ristic on 3/19/16.
//  Copyright © 2016 Parse. All rights reserved.
//

import UIKit
import Parse

public enum UserError : ErrorType {
    case BadLogin
    case CreateUserError
    case UpdateUserError
}

class UserAgent: NSObject
{

    func createUser(username:String, password:String, completion:((PFUser?, ErrorType?)->Void)?)
    {
        guard (username.characters.count != 0 && password.characters.count != 0) else {
            completion?(nil, UserError.CreateUserError)
            return
        }
        
        let newUser = PFUser()
        newUser.username = username
        newUser.password = password
        
        do {
            try newUser.signUp()
        }
        catch {
            completion?(newUser, UserError.CreateUserError)
        }
        
        newUser.saveInBackgroundWithBlock({ (success, error) -> Void in
            
            let err:UserError? = success ? nil : UserError.CreateUserError
            completion?(newUser, err)
        })

    }
    
    func logInUser(userId:String, password:String) throws -> PFUser?
    {
        guard (userId.characters.count != 0 && password.characters.count != 0) else {
            throw UserError.BadLogin
        }
        
        do{
            let usr =  try PFUser.logInWithUsername(userId, password: password)
            return usr
        }
        catch
        {
            throw UserError.BadLogin
        }
    }
    
    func logOutUser(user:AnyObject)
    {
        
    }
    
    func updateUser(user:AnyObject)
    {
        
    }
    
    func deleteUser(userId:String)
    {
    
    }
    
}
