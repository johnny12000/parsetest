//
//  PhotoAgent.swift
//  ParseStarterProject-Swift
//
//  Created by Nikola Ristic on 3/19/16.
//  Copyright © 2016 Parse. All rights reserved.
//

import UIKit
import Parse

public enum PhotoError : ErrorType {
    case RetrievePhotoError
    case CreatePhotoError
}

class PhotoAgent: NSObject
{
    func createPhoto(image:UIImage,
        user:PFUser,
        title:String,
        location:CLLocation?,
        heading:CLHeading?,
        completion:((AnyObject?, ErrorType?) -> Void)?)
    {
        let newImage = PFObject(className: "Photos")
        let file = PFFile(data: UIImageJPEGRepresentation(image, 1)!)
        let geoPoint = PFGeoPoint(location: location)
        let heading = heading?.trueHeading
        if let file = file {
            newImage.setObject(file, forKey:"photo")
        }
        newImage.setObject(title, forKey:"name")
        newImage.setObject(geoPoint, forKey: "Location")
        if let heading = heading {
            newImage.setObject(heading, forKey: "heading")
        }
        newImage.setObject(user, forKey: "user")
        
        let acl = PFACL(user: user)
        acl.publicReadAccess = true
        newImage.ACL = acl;

        newImage.saveInBackgroundWithBlock() { (success, error) -> Void in
            completion?(newImage, error)
        }
    }
    
    func updatePhoto(photo:AnyObject) -> AnyObject?
    {
        return nil
    }
    
    func retrievePhotos(user:PFUser, completion:(([AnyObject]?, ErrorType?) ->Void)?)
    {
        let query = PFQuery(className: "Photos")
        query.whereKey("user", equalTo: user)
        
        query.findObjectsInBackgroundWithBlock() { (objects, error) -> Void in
            
            completion?(objects, nil)
        }
    }
    
    func deletePhoto(photo:AnyObject)
    {
        
    }
    
}
